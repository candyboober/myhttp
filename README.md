## Myhttp

A Go http request library that allows to perform requests parallel and prints response hash.

### How to use.
- Install go compiler:
https://golang.org/doc/install

- Run compile command:
`go build myhttp.go`
  
- Run bin file: `./myhttp [ARGS]`

Myhttp takes the following arguments:
- `-parallel`, it's an integer value that sets goroutine pool size for parallelism. Note, the value must be (0, 1000]. By default is 10.

### How to run tests:
- Run the following command: `go test ./...`

### Examples:
- `myhttp http://github.com` will perform request to github.com and print hash.
- `myhttp http://github.com http://gitlab.com` will perform requests to github.com and gitlab.com and print their hash. Note, requests will performed unordered so hash will be printed unordered too.
- `myhttp -parallel 2 http://github.com http://gitlab.com http://bitbucket.com` will perform http request to each url, but parallel only 2, so it's will finish first 2 requests and after the last one.
- `myhttp http://github.com -parallel 2 http://gitlab.com` it's incorrect command, you must provide '-parallel' flag first, then urls.
