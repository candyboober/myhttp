package main

import (
	"context"
	"fmt"
	"log"
	"myhttp/client"
	"myhttp/config"
	"myhttp/parallel"
	"net/http"
)

func main() {
	args, err := config.New()
	if err != nil {
		log.Fatalf("can not build config: %s", err.Error())
	}

	httpClient := client.New(http.DefaultClient)
	httpParallel := parallel.New(httpClient, args.Parallel)
	response, err := httpParallel.Send(context.Background(), args.Urls)
	if err != nil {
		log.Fatalf("can not send parallel requests: %s", err.Error())
	}

	fmt.Println("Requests performed successfully")
	for i := range response.Hashes {
		fmt.Printf("# %s\n", response.Hashes[i])
	}
}
