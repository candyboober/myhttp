// parallel_test provides few unit tests on parallel client
// usualy I'm using testify: https://github.com/stretchr/testify
package parallel_test

import (
	"context"
	"myhttp/mocks"
	"myhttp/parallel"
	"net/url"
	"testing"
)

func TestParallelSuccessful(t *testing.T) {
	mock := &mocks.HttpMock{}
	p := parallel.New(mock, 10)
	u, err := url.Parse("http://yandex.com")
	if err != nil {
		t.Fatalf("can not parse url: %s", err)
	}
	res, err := p.Send(context.Background(), []*url.URL{u})
	if err != nil {
		t.Fatalf("error on send parallel requests: %s", err.Error())
	}

	if len(res.Hashes) != 1 {
		t.Fatalf("hashes length expected as 1, but given %d", len(res.Hashes))
	}
	if len(res.Hashes[0]) == 0 {
		t.Fatalf("given hash is empty")
	}
}

func TestParallelErrorOnHttpClient(t *testing.T) {
	mock := &mocks.HttpErrorMock{}
	p := parallel.New(mock, 10)
	u, err := url.Parse("http://yandex.com")
	if err != nil {
		t.Fatalf("can not parse url: %s", err)
	}
	_, err = p.Send(context.Background(), []*url.URL{u})
	if err == nil {
		t.Fatalf("expected error from http client, but given nil")
	}
}
