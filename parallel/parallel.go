package parallel

import (
	"context"
	"crypto/md5"
	"fmt"
	"net/url"
	"sync"
)

type HttpClient interface {
	Get(ctx context.Context, url *url.URL) ([]byte, error)
}

type Parallel struct {
	client HttpClient
	pool   int
}

func New(client HttpClient, pool int) *Parallel {
	return &Parallel{client: client, pool: pool}
}

type msg struct {
	Hash string
	Url  *url.URL
	Err  error
}

func (p *Parallel) Send(ctx context.Context, urls []*url.URL) (Response, error) {
	if len(urls) == 0 {
		return Response{}, nil
	}
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	responseCh := make(chan msg)
	var wg sync.WaitGroup
	wg.Add(len(urls))

	poolQueue := make(chan struct{}, p.pool)
	defer close(poolQueue)

	go func() {
		wg.Wait()
		close(responseCh)
	}()

	for i := range urls {
		go func(netUrl *url.URL) {
			poolQueue <- struct{}{}
			defer wg.Done()

			body, err := p.client.Get(ctx, netUrl)
			if err != nil {
				cancel()
				responseCh <- msg{
					Err: err,
					Url: netUrl,
				}
			}

			hash := md5.Sum(body)
			responseCh <- msg{
				Hash: string(hash[:]),
			}
		}(urls[i])
	}

	hashes := make([]string, 0, len(urls))
	for msg := range responseCh {
		if msg.Err != nil {
			return Response{}, fmt.Errorf("error occured due to http request on '%s': %w", msg.Url, msg.Err)
		}
		hashes = append(hashes, msg.Hash)
	}

	return Response{
		Hashes: hashes,
	}, nil
}
