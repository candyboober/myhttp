package client

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Http struct {
	c *http.Client
}

func New(c *http.Client) *Http {
	return &Http{c: c}
}

func (h *Http) Get(ctx context.Context, url *url.URL) ([]byte, error) {
	req, err := http.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("can not create http request: %w", err)
	}
	req = req.WithContext(ctx)
	res, err := h.c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("can not perform http request: %w", err)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("can not read http response body: %w", err)
	}

	return body, nil
}
