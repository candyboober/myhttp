package mocks

import (
	"context"
	"net/http"
	"net/url"
)

// HttpMock provides HttpMock interface stub
// usually I'm using gomock instead: https://github.com/golang/mock
type HttpMock struct {
}

func (h *HttpMock) Get(ctx context.Context, url *url.URL) ([]byte, error) {
	return []byte("mock response"), nil
}

type HttpErrorMock struct {
}

func (h *HttpErrorMock) Get(ctx context.Context, url *url.URL) ([]byte, error) {
	return nil, http.ErrHandlerTimeout
}
