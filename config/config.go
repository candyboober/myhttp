package config

import (
	"flag"
	"fmt"
	"log"
	"net/url"
)

const (
	defaultParallel = 10
	maxParallel     = 1000
)

// Args describes input arguments by command line
type Args struct {
	// Parallel sets pool size of goroutines that performs http requests
	Parallel int
	Urls     []*url.URL
}

func New() (Args, error) {
	var parallel int
	flag.IntVar(&parallel, "parallel", defaultParallel,
		"pool size of goroutines that performs http requests")
	flag.Parse()
	urls := flag.Args()
	if urls[0] == "-parallel" {
		urls = urls[2:]
	}
	netUrls := make([]*url.URL, len(urls))
	for i := range urls {
		netUrl, err := url.ParseRequestURI(urls[i])
		if err != nil {
			return Args{}, fmt.Errorf("given invalid url: %w", err)
		}

		netUrls[i] = netUrl
	}

	if parallel == 0 {
		parallel = defaultParallel
		log.Printf("WARN: you can not set parallel as 0, sets %d as default instead\n", defaultParallel)
	}
	if parallel > maxParallel {
		parallel = defaultParallel
		log.Printf("WARN: you can not set parallel larger then %d, sets %d as default instead",
			maxParallel, defaultParallel)
	}
	return Args{
		Parallel: parallel,
		Urls:     netUrls,
	}, nil
}
